$(document).ready(function() {
    var table = $('#example').dataTable( {

        "ajax": {
            //"type": "POST",
            "url": "exercise_data.json",
            "data"   : function( d ) {
                d.example_key1= $('#name').val();
                d.example_key2= $('#city').val();
                d.example_key3= $('#country').val();
            },
            "dataSrc": ""
        },
        "columns": [
            { "data": "name" },
            { "data": "city" },
            { "data": "country" },
            { "data": "company" },
            { "data": "job_history" },
            { "data": "email" }
        ]
    } );

    table.ajax.reload();

    //$('#reload').click(function () {
    //    table.ajax.reload(function(data){
    //        console.log(data);
    //    }, false);
    //} );


    //https://datatables.net/reference/api/ajax.reload
//    setInterval(function () {
//        table.ajax.reload(null, false);
//    }, 3000);
//} );

    $('#reload').click(function () {
        table.ajax.reload(null, false);
    }, 3000);


});