from flask import Flask, render_template
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_DIRS = os.path.join(BASE_DIR, 'templates')

app = Flask(__name__, static_url_path='')

@app.route('/')
def show_contacts():
    return render_template('contacts.html')

if __name__ == '__main__':
    app.run(debug=True)
