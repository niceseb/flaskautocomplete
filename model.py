from sqlalchemy import Column, Integer, String
from flask.ext.appbuilder import Model

class Contacts(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=False)
    city = Column(String(50), unique=False)
    country = Column(String(50), unique=False)
    company = Column(String(50), unique=False)
    job_history = Column(String(50), unique=False)
    email = Column(String(100), unique=False)

    def __repr__(self):
        return self.name